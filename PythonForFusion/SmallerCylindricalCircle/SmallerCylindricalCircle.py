import adsk.core, adsk.fusion, traceback

def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui = app.userInterface

        # Create a document.
        doc = app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)

        # Get the root component of the active design
        rootComp = design.rootComponent

        # Create sketch
        sketches = rootComp.sketches
        sketch = sketches.add(rootComp.xZConstructionPlane)
        
        # Create sketch circle
        sketchCircles = sketch.sketchCurves.sketchCircles
        centerPoint = adsk.core.Point3D.create(0, 0, 0)
        sketchCircles.addByCenterRadius(centerPoint, 1.45)        
        
        # Get the profile defined by the circle
        prof = sketch.profiles.item(0)

        # Define that the extent is a extent of 6 cm.
        distance = adsk.core.ValueInput.createByReal(6)
       
        # Create an extrusion input
        extrudes = rootComp.features.extrudeFeatures
        extInput = extrudes.createInput(prof, adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
        
        # Define that the extent is a distance extent of 6 cm
        distance = adsk.core.ValueInput.createByReal(4)
        # Set the distance extent to be symmetric
        extInput.setDistanceExtent(True, distance)
        # Set the extrude to be a solid one
        extInput.isSolid = True
        
        # Create an cylinder
        extrude = extrudes.add(extInput)

        # Create sketch line
        sketchLines = sketch.sketchCurves.sketchLines
        startPoint = adsk.core.Point3D.create(5, 5, 0)
        endPoint = adsk.core.Point3D.create(5, 10, 0)
        sketchLineOne = sketchLines.addByTwoPoints(startPoint, endPoint)
        endPointTwo = adsk.core.Point3D.create(10, 5, 0)
        sketchLineTwo = sketchLines.addByTwoPoints(startPoint, endPointTwo)
        
        # Create three sketch points
        sketchPoints = sketch.sketchPoints
        positionOne = adsk.core.Point3D.create(0, 5.0, 0)
        sketchPointOne = sketchPoints.add(positionOne)
        positionTwo = adsk.core.Point3D.create(5.0, 0, 0)
        sketchPointTwo = sketchPoints.add(positionTwo)
        positionThree = adsk.core.Point3D.create(0, -5.0, 0)
        sketchPointThree = sketchPoints.add(positionThree)

       

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
