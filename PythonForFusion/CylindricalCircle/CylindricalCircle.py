import adsk.core, adsk.fusion, traceback

def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui = app.userInterface

        # Create a document.
        doc = app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)

        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)

        # Get the root component of the active design
        rootComp = design.rootComponent

        # Create sketch
        sketches = rootComp.sketches
        sketch = sketches.add(rootComp.xZConstructionPlane)
        
        # Create sketch circle
        sketchCircles = sketch.sketchCurves.sketchCircles
        centerPoint = adsk.core.Point3D.create(0, 0, 0)
        sketchCircles.addByCenterRadius(centerPoint, 5.0)        
        
        # Get the profile defined by the circle
        prof = sketch.profiles.item(0)

        # Define that the extent is a distance extent of 10 cm.
        distance = adsk.core.ValueInput.createByReal(10)
       
        # Create an extrusion input
        extrudes = rootComp.features.extrudeFeatures
        extInput = extrudes.createInput(prof, adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
        
        # Define that the extent is a distance extent of 10 cm
        distance = adsk.core.ValueInput.createByReal(10)
        # Set the distance extent to be symmetric
        extInput.setDistanceExtent(True, distance)
        # Set the extrude to be a solid one
        extInput.isSolid = True
        
        # Create an cylinder
        extrude = extrudes.add(extInput)

        # Create sketch line
        sketchLines = sketch.sketchCurves.sketchLines
        startPoint = adsk.core.Point3D.create(5, 5, 0)
        endPoint = adsk.core.Point3D.create(5, 10, 0)
        sketchLineOne = sketchLines.addByTwoPoints(startPoint, endPoint)
        endPointTwo = adsk.core.Point3D.create(10, 5, 0)
        sketchLineTwo = sketchLines.addByTwoPoints(startPoint, endPointTwo)
        
        # Create three sketch points
        sketchPoints = sketch.sketchPoints
        positionOne = adsk.core.Point3D.create(0, 5.0, 0)
        sketchPointOne = sketchPoints.add(positionOne)
        positionTwo = adsk.core.Point3D.create(5.0, 0, 0)
        sketchPointTwo = sketchPoints.add(positionTwo)
        positionThree = adsk.core.Point3D.create(0, -5.0, 0)
        sketchPointThree = sketchPoints.add(positionThree)
        
        # Get construction planes
        planes = rootComp.constructionPlanes
        
        # Create construction plane input
        planeInput = planes.createInput()
        
        cylinderFace = extrude.sideFaces.item(0)
        
        # Add construction plane by tangent at point
        planeInput.setByTangentAtPoint(cylinderFace, sketchPointOne)
        offsetPlane = planes.add(planeInput)

        #Create a sketch ont construction plane and add two sketch points on it
        offsetSketch = sketches.add(offsetPlane)
        offsetSketchPoints = offsetSketch.sketchPoints
        sPt0 = offsetSketchPoints.add(adsk.core.Point3D.create(0,-7,0))
        sPt1 = offsetSketchPoints.add(adsk.core.Point3D.create(0,7,0))

        #Add two sketch points into a collection
        ptColl = adsk.core.ObjectCollection.create()
        ptColl.add(sPt0)
        ptColl.add(sPt1)

        #Create a hole input
        holes = rootComp.features.holeFeatures
        holeInput = holes.createSimpleInput(adsk.core.ValueInput.createByString('25 mm'))
        holeInput.setPositionBySketchPoints(ptColl)
        holeInput.setDistanceExtent(distance)

        hole = holes.add(holeInput)
        
       

    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
