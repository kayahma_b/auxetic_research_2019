import adsk.core, adsk.fusion, traceback

def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        # ui = app.userInterface
        
        # Create a document.
        # doc = app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)
 
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)

        # Get the root component of the active design.
        rootComp = design.rootComponent
        
        # Create sketch
        sketches = rootComp.sketches
        sketch = sketches.add(rootComp.xYConstructionPlane)
        
         
        # Draw a rectangle by two points.
        dimRec=20
        lines = sketch.sketchCurves.sketchLines;
        recLines = lines.addCenterPointRectangle(adsk.core.Point3D.create(0, 0, 0), 
                                                 adsk.core.Point3D.create(dimRec/4, dimRec/8, 0))
        
        # Get the profile defined by the circle.
        prof = sketch.profiles.item(0)

        
         
        # Create distance value inputs
        mm10 = adsk.core.ValueInput.createByString("100mm" )
        
        #Get extrude features
        features = rootComp.features
        extrudes = features.extrudeFeatures
        extInput = extrudes.createInput(prof,
                            adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
        extInput.setDistanceExtent(False, adsk.core.ValueInput.createByReal(0.8))
        extFront = extrudes.add(extInput)
        
        bodyFront=extFront.bodies.item(0)
        bodyFront.name= "Frontplatte"
     
        # Create a distance extent definition
        extent_distance_2 = adsk.fusion.DistanceExtentDefinition.create(mm10)
        
        # Create a start extent that starts from a brep face with an offset of 10 mm.
        endsurf = extFront.endFaces
        start_from = adsk.fusion.FromEntityStartDefinition.create(endsurf.item(0), mm10)


        # Create a construction plane by offsetting the end face
        planes = rootComp.constructionPlanes
        planeInput = planes.createInput()
        offsetVal = adsk.core.ValueInput.createByString("20 mm")
        planeInput.setByOffset(prof, offsetVal)
        offsetPlane = planes.add(planeInput)
        
        # Create a sketch on the new construction plane and add four sketch points on it
        offsetSketch = sketches.add(offsetPlane)
        offsetSketchPoints = offsetSketch.sketchPoints
        sPt0 = offsetSketchPoints.add(adsk.core.Point3D.create(3, 0, 0))
        sPt1 = offsetSketchPoints.add(adsk.core.Point3D.create(-3, 0, 0))
      
        
        # Add the four sketch points into a collection
        ptColl = adsk.core.ObjectCollection.create()
        ptColl.add(sPt0)
        ptColl.add(sPt1)
      
        
        # Create a hole input
        holes = rootComp.features.holeFeatures
        holeInput = holes.createSimpleInput(adsk.core.ValueInput.createByString("30 mm"))
        holeInput.setPositionBySketchPoints(ptColl)
        holeInput.setDistanceExtent(mm10)
        
        hole = holes.add(holeInput)
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
