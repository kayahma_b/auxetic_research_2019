import adsk.core, adsk.fusion, traceback

# Constants

# dimension of rectangle
DIMREC=10
TR=11
# distance value inputs
EXTRUSIONSIZE="100mm"



def addRectangleToSketch(sketch,cx,cy,cz,px,py,pz):
    lines = sketch.sketchCurves.sketchLines;
    recLines = lines.addCenterPointRectangle(adsk.core.Point3D.create(cx,cy,cz), 
                                             adsk.core.Point3D.create(px,py,pz))
    return sketch
    
def extrudeSomething(rootComp,sketch,i):

    prof = sketch.profiles.item(i)
    
    # # Create distance value inputs
    # mm10 = adsk.core.ValueInput.createByString(EXTRUSIONSIZE)


    #Get extrude features
    features = rootComp.features
    extrudes = features.extrudeFeatures
    extInput = extrudes.createInput(prof,
                        adsk.fusion.FeatureOperations.NewBodyFeatureOperation)
    extInput.setDistanceExtent(False, adsk.core.ValueInput.createByReal(0.8))
    extFront = extrudes.add(extInput)

    bodyFront=extFront.bodies.item(0)
    bodyFront.name= "Frontplatte"
    
    return prof

def run(context):
    ui = None
    try:
        # Some constants
        mm10 = adsk.core.ValueInput.createByString(EXTRUSIONSIZE)
            
        app = adsk.core.Application.get()
        # ui = app.userInterface
        
        # Create a document.
        # doc = app.documents.add(adsk.core.DocumentTypes.FusionDesignDocumentType)
 
        product = app.activeProduct
        design = adsk.fusion.Design.cast(product)

        # Get the root component of the active design.
        rootComp = design.rootComponent
        
        # Create sketch
        sketches = rootComp.sketches
        sketch = sketches.add(rootComp.xYConstructionPlane)
        
        # add two rectangles on the sketch                                         
        sketch=addRectangleToSketch(sketch,0,0,0,DIMREC/2, DIMREC/10, 0)
        sketch=addRectangleToSketch(sketch,TR,0,0, TR+DIMREC/2, DIMREC/10, 0)
        
        # Extrude the two rectangles.
        prof0=extrudeSomething(rootComp,sketch,0)
        prof1=extrudeSomething(rootComp,sketch,1)
        
        # RESUME FROM HERE

        # Create a distance extent definition
        extent_distance_2 = adsk.fusion.DistanceExtentDefinition.create(mm10)

        # Create a start extent that starts from a brep face with an offset of 10 mm.
        # endsurf = extFront.endFaces
        # start_from = adsk.fusion.FromEntityStartDefinition.create(endsurf.item(0), mm10)


        # Create a construction plane by offsetting the end face
        planes = rootComp.constructionPlanes
        planeInput = planes.createInput()
        offsetVal = adsk.core.ValueInput.createByString("20 mm")
        planeInput.setByOffset(prof0, offsetVal)
        offsetPlane = planes.add(planeInput)

        # Create a sketch on the new construction plane and add two sketch points on it
        offsetSketch = sketches.add(offsetPlane)
        offsetSketchPoints = offsetSketch.sketchPoints
        sPt0 = offsetSketchPoints.add(adsk.core.Point3D.create(3, 0, 0))
        sPt1 = offsetSketchPoints.add(adsk.core.Point3D.create(-3, 0, 0))
      
        
        # Add the two sketch points into a collection
        ptColl = adsk.core.ObjectCollection.create()
        ptColl.add(sPt0)
        ptColl.add(sPt1)
      
        
        # Create a hole input
        holes = rootComp.features.holeFeatures
        holeInput = holes.createSimpleInput(adsk.core.ValueInput.createByString("30 mm"))
        holeInput.setPositionBySketchPoints(ptColl)
        holeInput.setDistanceExtent(mm10)

        hole = holes.add(holeInput)
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))
