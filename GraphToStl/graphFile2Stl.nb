(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 12.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       158,          7]
NotebookDataLength[      9631,        269]
NotebookOptionsPosition[      7505,        225]
NotebookOutlinePosition[      7847,        240]
CellTagsIndexPosition[      7804,        237]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["From Graph to Graphics to Stl", "Title",
 CellChangeTimes->{{3.7804185706200953`*^9, 
  3.780418600010258*^9}},ExpressionUUID->"5c99006c-b13a-4125-9347-\
df5cdd421917"],

Cell["\<\
Ileana Streinu
Oct 21, 2019\
\>", "Subtitle",
 CellChangeTimes->{{3.7804186089428616`*^9, 3.7804186181637783`*^9}, {
  3.7806741306782303`*^9, 
  3.780674132092448*^9}},ExpressionUUID->"66c0dddd-8da1-42ec-bc87-\
ca06e9a8537c"],

Cell[BoxData[
 RowBox[{
  RowBox[{"FILENAME", "=", "\"\<graph.txt\>\""}], ";"}]], "Input",
 CellChangeTimes->{{3.7806727602021694`*^9, 3.780672789615428*^9}},
 CellLabel->
  "(Debug) In[12]:=",ExpressionUUID->"51528550-8e83-4fd6-bde0-c8e0ffe97ed5"],

Cell[CellGroupData[{

Cell["Graphics for Graph", "Section",
 CellChangeTimes->{{3.7804186260938487`*^9, 3.7804186303421307`*^9}, 
   3.7804186659650393`*^9, {3.780419225892286*^9, 
   3.780419229756797*^9}},ExpressionUUID->"abc4b750-76eb-4f59-a68f-\
43185ca3da92"],

Cell[CellGroupData[{

Cell["Constants", "Subsection",
 CellChangeTimes->{{3.7804192783472614`*^9, 
  3.7804192959219446`*^9}},ExpressionUUID->"2c506c4a-a398-4d70-9f4c-\
63d1c31e204e"],

Cell[BoxData[{
 RowBox[{
  RowBox[{"SPHERERADIUS", "=", "0.1"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"TUBERADIUS", "=", "0.05"}], ";"}]}], "Input",
 CellChangeTimes->{{3.78041929907313*^9, 3.780419304574306*^9}, {
  3.780419350374419*^9, 3.780419371494261*^9}, {3.7804200117231007`*^9, 
  3.7804200173039336`*^9}},
 CellLabel->
  "(Debug) In[13]:=",ExpressionUUID->"21ad29e8-ce97-4c4e-af42-6b8321f3fdac"]
}, Open  ]],

Cell[CellGroupData[{

Cell["Draw objects", "Subsection",
 CellChangeTimes->{{3.7804192783472614`*^9, 3.7804192959219446`*^9}, {
  3.7804193965255547`*^9, 
  3.7804194000220985`*^9}},ExpressionUUID->"631c6242-5d6c-492c-966c-\
e6b01768ed2c"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"drawVertices", "[", "pts_", "]"}], ":=", 
   RowBox[{"Sphere", "[", 
    RowBox[{"pts", ",", "SPHERERADIUS"}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7804194021080723`*^9, 3.780419438893507*^9}},
 CellLabel->
  "(Debug) In[15]:=",ExpressionUUID->"64ff8c71-d21c-4a3c-b122-97626e3ecf95"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"drawEdges", "[", 
    RowBox[{"pts_", ",", "edges_"}], "]"}], ":=", 
   RowBox[{"GraphicsComplex", "[", 
    RowBox[{"pts", ",", 
     RowBox[{"Cylinder", "[", 
      RowBox[{"edges", ",", "TUBERADIUS"}], "]"}]}], "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.780419605207078*^9, 3.780419672486032*^9}},
 CellLabel->
  "(Debug) In[16]:=",ExpressionUUID->"4e9c6dab-e391-4f0b-8406-ac8fa396a638"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"drawGraph", "[", 
    RowBox[{"{", 
     RowBox[{"pts_", ",", "edges_"}], "}"}], "]"}], ":=", 
   RowBox[{"{", 
    RowBox[{
     RowBox[{"drawVertices", "[", "pts", "]"}], ",", 
     RowBox[{"drawEdges", "[", 
      RowBox[{"pts", ",", "edges"}], "]"}]}], "}"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7804197379872866`*^9, 3.780419790909209*^9}, {
  3.7804202781318893`*^9, 3.780420281357957*^9}},
 CellLabel->
  "(Debug) In[17]:=",ExpressionUUID->"9f4016ab-47b3-44a0-9e45-6ea16b49343d"],

Cell[CellGroupData[{

Cell["UnitTests", "Subsubsection",
 CellChangeTimes->{{3.780419530363223*^9, 
  3.7804195420390472`*^9}},ExpressionUUID->"d7993070-d514-447e-b2d4-\
2bb837845945"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"Graphics3D", "[", 
   RowBox[{"drawVertices", "[", "PTS", "]"}], "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7804195505471935`*^9, 3.7804195663529153`*^9}, {
  3.780674033643567*^9, 3.7806740404373984`*^9}},
 CellLabel->
  "(Debug) In[18]:=",ExpressionUUID->"746a8d73-d638-4409-8daa-7e1fdb7d6acd"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"Graphics3D", "[", 
   RowBox[{"drawEdges", "[", 
    RowBox[{"PTS", ",", "EDGES"}], "]"}], "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.780419721887867*^9, 3.780419725358259*^9}, {
  3.7806740481527624`*^9, 3.7806740543242617`*^9}},
 CellLabel->
  "(Debug) In[19]:=",ExpressionUUID->"bd32dc18-2ed1-4f90-940d-cbe5871a7c91"],

Cell[BoxData[
 RowBox[{"(*", 
  RowBox[{"Graphics3D", "[", 
   RowBox[{"drawGraph", "[", "GRAPH", "]"}], "]"}], "*)"}]], "Input",
 CellChangeTimes->{{3.7804198024177303`*^9, 3.7804198145449877`*^9}, {
  3.78067406733147*^9, 3.780674073036216*^9}},
 CellLabel->
  "(Debug) In[20]:=",ExpressionUUID->"eafc4c2d-1709-4ae9-87e7-86a639ee7d54"]
}, Open  ]]
}, Open  ]]
}, Closed]],

Cell[CellGroupData[{

Cell["Read, make graphics and export", "Section",
 CellChangeTimes->{{3.7804203487379093`*^9, 3.780420364463993*^9}, {
  3.780605560878689*^9, 
  3.7806055633601055`*^9}},ExpressionUUID->"e788e218-669f-447a-a7c5-\
3eb165c33f2e"],

Cell[BoxData[
 RowBox[{
  RowBox[{
   RowBox[{"readGraphFileMakeGraphicsExportToStl", "[", 
    RowBox[{"inputFileName_", ",", "outputStlFileName_"}], "]"}], ":=", 
   RowBox[{"Module", "[", 
    RowBox[{
     RowBox[{"{", 
      RowBox[{"graphStream", ",", "pts", ",", "edges", ",", "gr"}], "}"}], 
     ",", "\[IndentingNewLine]", 
     RowBox[{
      RowBox[{"graphStream", "=", 
       RowBox[{"OpenRead", "[", 
        RowBox[{
         RowBox[{"NotebookDirectory", "[", "]"}], "<>", "inputFileName"}], 
        "]"}]}], ";", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{"pts", ",", "edges"}], "}"}], "=", 
       RowBox[{"Read", "[", "graphStream", "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"Close", "[", "graphStream", "]"}], ";", "\[IndentingNewLine]", 
      RowBox[{"gr", "=", 
       RowBox[{"Graphics3D", "[", 
        RowBox[{"drawGraph", "[", 
         RowBox[{"{", 
          RowBox[{"pts", ",", "edges"}], "}"}], "]"}], "]"}]}], ";", 
      "\[IndentingNewLine]", 
      RowBox[{"Export", "[", 
       RowBox[{
        RowBox[{
         RowBox[{"NotebookDirectory", "[", "]"}], "<>", "outputStlFileName"}],
         ",", " ", "gr", ",", 
        RowBox[{"{", 
         RowBox[{"\"\<STL\>\"", ",", 
          RowBox[{"\"\<BinaryFormat\>\"", "\[Rule]", " ", "False"}]}], 
         "}"}]}], "]"}], ";"}]}], "\[IndentingNewLine]", 
    "\[IndentingNewLine]", "]"}]}], ";"}]], "Input",
 CellChangeTimes->{{3.7806730623410378`*^9, 3.78067327444444*^9}, {
  3.780673329650797*^9, 3.780673369244953*^9}, {3.7806734263128386`*^9, 
  3.780673525067724*^9}},
 CellLabel->
  "(Debug) In[21]:=",ExpressionUUID->"5a4b933d-5edc-42d3-8924-f854a03cc332"]
}, Closed]],

Cell[CellGroupData[{

Cell["Run", "Section",
 CellChangeTimes->{{3.780673555169222*^9, 3.7806735711584587`*^9}, {
  3.7806740993031445`*^9, 
  3.7806741002426295`*^9}},ExpressionUUID->"4f53f73d-5aed-4ef4-b2e6-\
a4e4d1c1c484"],

Cell[BoxData[
 RowBox[{"readGraphFileMakeGraphicsExportToStl", "[", 
  RowBox[{"\"\<graph.txt\>\"", ",", "\"\<graph.stl\>\""}], "]"}]], "Input",
 CellChangeTimes->{{3.780673576405429*^9, 3.78067359303994*^9}},
 CellLabel->
  "(Debug) In[22]:=",ExpressionUUID->"3f1c2e85-d9f6-4d4a-86c3-4e6254271e71"]
}, Open  ]]
}, Open  ]]
},
WindowSize->{1264, 637},
WindowMargins->{{-8, Automatic}, {Automatic, 0}},
FrontEndVersion->"12.0 for Microsoft Windows (64-bit) (April 8, 2019)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[580, 22, 174, 3, 96, "Title",ExpressionUUID->"5c99006c-b13a-4125-9347-df5cdd421917"],
Cell[757, 27, 236, 7, 85, "Subtitle",ExpressionUUID->"66c0dddd-8da1-42ec-bc87-ca06e9a8537c"],
Cell[996, 36, 248, 5, 42, "Input",ExpressionUUID->"51528550-8e83-4fd6-bde0-c8e0ffe97ed5"],
Cell[CellGroupData[{
Cell[1269, 45, 242, 4, 67, "Section",ExpressionUUID->"abc4b750-76eb-4f59-a68f-43185ca3da92"],
Cell[CellGroupData[{
Cell[1536, 53, 161, 3, 53, "Subsection",ExpressionUUID->"2c506c4a-a398-4d70-9f4c-63d1c31e204e"],
Cell[1700, 58, 419, 9, 48, "Input",ExpressionUUID->"21ad29e8-ce97-4c4e-af42-6b8321f3fdac"]
}, Open  ]],
Cell[CellGroupData[{
Cell[2156, 72, 217, 4, 53, "Subsection",ExpressionUUID->"631c6242-5d6c-492c-966c-e6b01768ed2c"],
Cell[2376, 78, 341, 8, 28, "Input",ExpressionUUID->"64ff8c71-d21c-4a3c-b122-97626e3ecf95"],
Cell[2720, 88, 442, 11, 28, "Input",ExpressionUUID->"4e9c6dab-e391-4f0b-8406-ac8fa396a638"],
Cell[3165, 101, 538, 14, 28, "Input",ExpressionUUID->"9f4016ab-47b3-44a0-9e45-6ea16b49343d"],
Cell[CellGroupData[{
Cell[3728, 119, 162, 3, 44, "Subsubsection",ExpressionUUID->"d7993070-d514-447e-b2d4-2bb837845945"],
Cell[3893, 124, 341, 7, 28, "Input",ExpressionUUID->"746a8d73-d638-4409-8daa-7e1fdb7d6acd"],
Cell[4237, 133, 365, 8, 28, "Input",ExpressionUUID->"bd32dc18-2ed1-4f90-940d-cbe5871a7c91"],
Cell[4605, 143, 337, 7, 28, "Input",ExpressionUUID->"eafc4c2d-1709-4ae9-87e7-86a639ee7d54"]
}, Open  ]]
}, Open  ]]
}, Closed]],
Cell[CellGroupData[{
Cell[5003, 157, 228, 4, 53, "Section",ExpressionUUID->"e788e218-669f-447a-a7c5-3eb165c33f2e"],
Cell[5234, 163, 1701, 42, 162, "Input",ExpressionUUID->"5a4b933d-5edc-42d3-8924-f854a03cc332"]
}, Closed]],
Cell[CellGroupData[{
Cell[6972, 210, 203, 4, 53, "Section",ExpressionUUID->"4f53f73d-5aed-4ef4-b2e6-a4e4d1c1c484"],
Cell[7178, 216, 299, 5, 99, "Input",ExpressionUUID->"3f1c2e85-d9f6-4d4a-86c3-4e6254271e71"]
}, Open  ]]
}, Open  ]]
}
]
*)

